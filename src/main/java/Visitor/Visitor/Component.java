package Visitor.Visitor;

public interface Component {
	void traverse();
}
